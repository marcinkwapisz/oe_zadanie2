﻿using System;

namespace Zadanie2.BusinessLogic
{
    public class ObjectiveFunction
    {
        public enum FunctionTypeEnum { Rosenbrock, Griewank, Rastrigin };
        public static double GetFitnessToObjectiveFunction(FunctionTypeEnum functionType, double[] x)
        {
            switch (functionType)
            {
                case FunctionTypeEnum.Rastrigin:
                    {
                        double sum = 0, part1, part2;
                        for (int i = 0; i < x.Length; i++)
                        {
                            part1 = x[i] * x[i];
                            part2 = 10 * Math.Cos(2 * Math.PI * x[i]);
                            sum += part1 - part2 + 10;
                        }
                        return sum;
                    }
                case FunctionTypeEnum.Rosenbrock:
                    {
                        double sum = 0, part1, part2;
                        for (int i = 0; i < x.Length - 1; i++)
                        {
                            part1 = (x[i + 1] - x[i] * x[i]) * (x[i + 1] - x[i] * x[i]);
                            part2 = (x[i] - 1) * (x[i] - 1);
                            sum += (100 * part1 + part2);
                        }
                        return sum;
                    }
                case FunctionTypeEnum.Griewank:
                    {
                        double part1 = 0, part2 = 1;
                        for (int i = 0; i < x.Length; i++)
                        {
                            part1 += x[i] * x[i];

                        }
                        for (int i = 0; i < x.Length; i++)
                        {
                            part2 *= Math.Cos(x[i] / Math.Sqrt(i + 1));
                        }
                        return part1 / 4000 - part2 + 1;
                    }
                default:
                    return double.MaxValue;
            }
        }
    }
}
