﻿using MathNet.Numerics.Distributions;
using System.Collections.Generic;
using System.Linq;
using Zadanie2.Data;

namespace Zadanie2.BusinessLogic
{
    class MSPSO_Service
    {
        private readonly Parameters parameters;
        private Swarm swarm;
        private int bestAmountOfIterations = int.MaxValue;
        private Results results = new Results();
        private Normal normalDistribution = new Normal();

        public MSPSO_Service(Parameters parameters)
        {
            this.parameters = parameters;
        }

        public void OptimizeWithMSPSO()
        {
            List<double> averageFitnessInIteration = new List<double>();
            List<double> fitnessOfEachParticle = new List<double>();
            List<int> iterationNumberForParticles = new List<int>();
            swarm = new Swarm(new Particle[parameters.NumberOfParticles], new double[parameters.NumberOfDimensions], double.MaxValue);
            swarm.InitializeSwarm(parameters);
            Swarm[] subswarms = swarm.DivideSwarmIntoSubswarms(parameters.NumberOfParticlesInSubswarm);
            double[] averageFitnessInSubswarms = new double[subswarms.Length];
            Particle[] eliteParticlesFromAllSubswarms = SetEliteParticles(subswarms);
            int iteration;
            bool optimizationWasSuccessful = false;
            for (iteration = 1; iteration < parameters.NumberOfIterations; iteration++)
            {
                for (int i = 0; i < subswarms.Length; i++)
                {
                    OptimizeWithPSO(subswarms[i], eliteParticlesFromAllSubswarms);
                    averageFitnessInSubswarms[i] = CalculateFitnessAverageValue(subswarms[i]);
                    for (int j = 0; j < subswarms[i].Particles.Length; j++)
                    {
                        iterationNumberForParticles.Add(iteration);
                        fitnessOfEachParticle.Add(subswarms[i].Particles[j].Fitness);
                    }
                }
                eliteParticlesFromAllSubswarms = SetEliteParticles(subswarms);
                double averageFitnessInSwarm = averageFitnessInSubswarms.Average();
                averageFitnessInIteration.Add(averageFitnessInSwarm);
                if (averageFitnessInSwarm < parameters.Accuracy)
                {
                    optimizationWasSuccessful = true;
                    break;
                }
            }
            if (iteration < bestAmountOfIterations)
            {
                results.FitnessFromBestOptimizationRun = averageFitnessInIteration;
                results.IterationNumberForParticles = iterationNumberForParticles;
                results.FitnessOfEachParticle = fitnessOfEachParticle;
                bestAmountOfIterations = iteration;
            }
            results.NumberOfIterations.Add(iteration);
            results.IfOptimizationWasSuccessful.Add(optimizationWasSuccessful);
        }

        private Particle[] SetEliteParticles(Swarm[] subswarms)
        {
            Particle[] eliteParticles = new Particle[subswarms.Length];
            for (int i = 0; i < subswarms.Length; i++)
            {
                eliteParticles[i] = subswarms[i].Particles.OrderBy(x => x.BestFitness).First();
            }
            return eliteParticles;
        }

        private void OptimizeWithPSO(Swarm subswarm, Particle[] eliteParticlesFromAllSubswarms)
        {
            for (int i = 0; i < subswarm.Particles.Length; i++)
            {
                if (eliteParticlesFromAllSubswarms.Contains(subswarm.Particles[i]))
                {
                    UpdatePositionOfEliteParticle(subswarm.Particles[i], eliteParticlesFromAllSubswarms);
                }
                else
                {
                    UpdateVelocityOfParticle(subswarm, subswarm.Particles[i]);
                    UpdatePositionOfParticle(subswarm.Particles[i]);
                }
                CalculateFitnessOfParticle(subswarm, subswarm.Particles[i]);
            }
        }

        private void UpdatePositionOfEliteParticle(Particle particle, Particle[] eliteParticlesFromAllSubswarms)
        {
            Particle[] eliteParticles = eliteParticlesFromAllSubswarms.Except(new[] { particle }).ToArray();
            double[] newPosition = new double[particle.Position.Length];
            for (int j = 0; j < particle.Position.Length; j++)
            {
                double mean = 0.0;
                for (int k = 0; k < eliteParticles.Length; k++)
                {
                    mean += eliteParticles[k].BestPosition[j];
                }
                mean /= eliteParticles.Length;
                newPosition[j] = mean * (1.0 + normalDistribution.Sample());
            }
            newPosition.CopyTo(particle.Position, 0);
        }

        private void UpdateVelocityOfParticle(Swarm subswarm, Particle particle)
        {
            double[] newVelocity = new double[particle.Velocity.Length];
            for (int j = 0; j < particle.Velocity.Length; j++)
            {
                double random1 = HelperClass.GetRandomNumberFromRange(0.0, 1.0);
                double random2 = HelperClass.GetRandomNumberFromRange(0.0, 1.0);
                newVelocity[j] = (parameters.Weight * particle.Velocity[j]) +
                (parameters.CognitiveWeight * random1 * (particle.BestPosition[j] - particle.Position[j])) +
                (parameters.SocialWeight * random2 * (subswarm.BestGlobalPosition[j] - particle.Position[j]));
            }
            newVelocity.CopyTo(particle.Velocity, 0);
        }

        private void UpdatePositionOfParticle(Particle particle)
        {
            double[] newPosition = new double[particle.Position.Length];
            double sigma = HelperClass.GetRandomNumberFromRange(0, 1);
            if (sigma < 0.6)
            {
                for (int j = 0; j < particle.Position.Length; j++)
                    newPosition[j] = particle.Position[j] + particle.Velocity[j];
            }
            else
            {
                for (int j = 0; j < particle.Position.Length; j++)
                    newPosition[j] = particle.BestPosition[j] * (1.0 + normalDistribution.Sample());
            }
            newPosition.CopyTo(particle.Position, 0);
        }

        private void CalculateFitnessOfParticle(Swarm subswarm, Particle particle)
        {
            particle.Fitness = ObjectiveFunction.GetFitnessToObjectiveFunction(parameters.Function, particle.Position);
            if (particle.Fitness < particle.BestFitness)
            {
                particle.BestFitness = particle.Fitness;
                particle.Position.CopyTo(particle.BestPosition, 0);
                if (particle.BestFitness < subswarm.BestGlobalFitness)
                {
                    subswarm.BestGlobalFitness = particle.BestFitness;
                    particle.BestPosition.CopyTo(subswarm.BestGlobalPosition, 0);
                }
            }
        }

        private double CalculateFitnessAverageValue(Swarm subswarm)
        {
            double sum = 0.0;
            for (int i = 0; i < subswarm.Particles.Length; i++)
            {
                sum += subswarm.Particles[i].Fitness;
            }
            return sum / subswarm.Particles.Length;
        }

        public void SaveResultsOfOptimization()
        {
            results.SaveResultsToXml("MSPSO");
        }
    }
}
