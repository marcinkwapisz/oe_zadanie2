﻿using System;

namespace Zadanie2.BusinessLogic
{
    public static class HelperClass
    {
        private static Random random = new Random();

        public static double GetRandomNumberFromRange(double min, double max)
        {
            return random.NextDouble() * (max - min) + min;
        }

        public static int GetRandomIndexFromTable<T>(T[] array)
        {
            int n = array.Length;
            return random.Next(0, n);
        }

        public static void Shuffle<T>(T[] array)
        {
            int n = array.Length;
            while (n > 1)
            {
                int k = random.Next(n--);
                T temp = array[n];
                array[n] = array[k];
                array[k] = temp;
            }
        }
    }
}
