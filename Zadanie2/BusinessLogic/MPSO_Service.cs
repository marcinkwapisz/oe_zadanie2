﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zadanie2.Data;

namespace Zadanie2.BusinessLogic
{
    class MPSO_Service
    {
        private readonly Parameters parameters;
        private Swarm swarm;
        private int bestAmountOfIterations = int.MaxValue;
        private Results results = new Results();

        public MPSO_Service(Parameters parameters)
        {
            this.parameters = parameters;
        }

        public void OptimizeWithMPSO()
        {
            List<double> averageFitnessInIteration = new List<double>();
            List<double> fitnessOfEachParticle = new List<double>();
            List<int> iterationNumberForParticles = new List<int>();
            swarm = new Swarm(new Particle[parameters.NumberOfParticles], new double[parameters.NumberOfDimensions], double.MaxValue);
            swarm.InitializeSwarm(parameters);
            Swarm[] subswarms = swarm.DivideSwarmIntoSubswarms(parameters.NumberOfParticlesInSubswarm);
            double[] averageFitnessInSubswarms = new double[subswarms.Length];
            int iteration;
            bool optimizationWasSuccessful = false;
            for (iteration = 1; iteration < parameters.NumberOfIterations; iteration++)
            {
                for (int i = 0; i < subswarms.Length; i++)
                {
                    OptimizeWithPSO(subswarms[i]);
                    averageFitnessInSubswarms[i] = CalculateFitnessAverageValue(subswarms[i]);
                    for (int j = 0; j < subswarms[i].Particles.Length; j++)
                    {
                        iterationNumberForParticles.Add(iteration); 
                        fitnessOfEachParticle.Add(subswarms[i].Particles[j].Fitness);
                    }
                }
                double averageFitnessInSwarm = averageFitnessInSubswarms.Average();
                averageFitnessInIteration.Add(averageFitnessInSwarm);
                if (averageFitnessInSwarm < parameters.Accuracy)
                {
                    optimizationWasSuccessful = true;
                    break;
                }
                if (iteration % parameters.MPSOInterval == 0)
                {
                    CombineSubswarms(subswarms);
                    HelperClass.Shuffle(swarm.Particles);
                    subswarms = swarm.DivideSwarmIntoSubswarms(parameters.NumberOfParticlesInSubswarm);
                }
            }
            if (iteration < bestAmountOfIterations)
            {
                results.FitnessFromBestOptimizationRun = averageFitnessInIteration;
                results.IterationNumberForParticles = iterationNumberForParticles;
                results.FitnessOfEachParticle = fitnessOfEachParticle;
                bestAmountOfIterations = iteration;
            }
            results.NumberOfIterations.Add(iteration);
            results.IfOptimizationWasSuccessful.Add(optimizationWasSuccessful);
        }

        private void CombineSubswarms(Swarm[] subswarms)
        {
            for (int i = 0; i < subswarms.Length; i++)
            {
                for (int j = 0; j < subswarms[i].Particles.Length; j++)
                {
                    swarm.Particles[i * parameters.NumberOfParticlesInSubswarm + j] = subswarms[i].Particles[j];
                }
            }
            var subswarmWithBestFitness = subswarms.OrderBy(swarm => swarm.BestGlobalFitness).First();
            var index = Array.IndexOf(subswarms, subswarmWithBestFitness);
            swarm.BestGlobalFitness = subswarms[index].BestGlobalFitness;
            subswarms[index].BestGlobalPosition.CopyTo(swarm.BestGlobalPosition, 0);
        }

        private void OptimizeWithPSO(Swarm swarm)
        {
            for (int i = 0; i < swarm.Particles.Length; i++)
            {
                UpdateVelocityOfParticle(swarm, swarm.Particles[i]);
                UpdatePositionOfParticle(swarm.Particles[i]);
                CalculateFitnessOfParticle(swarm, swarm.Particles[i]);
            }
        }

        private void UpdateVelocityOfParticle(Swarm subswarm, Particle particle)
        {
            double[] newVelocity = new double[particle.Velocity.Length];
            for (int j = 0; j < particle.Velocity.Length; j++)
            {
                double random1 = HelperClass.GetRandomNumberFromRange(0.0, 1.0);
                double random2 = HelperClass.GetRandomNumberFromRange(0.0, 1.0);
                newVelocity[j] = (parameters.Weight * particle.Velocity[j]) +
                (parameters.CognitiveWeight * random1 * (particle.BestPosition[j] - particle.Position[j])) +
                (parameters.SocialWeight * random2 * (subswarm.BestGlobalPosition[j] - particle.Position[j]));
            }
            newVelocity.CopyTo(particle.Velocity, 0);
        }

        private void UpdatePositionOfParticle(Particle particle)
        {
            double[] newPosition = new double[particle.Position.Length];
            for (int j = 0; j < particle.Position.Length; j++)
            {
                newPosition[j] = particle.Position[j] + particle.Velocity[j];
            }
            newPosition.CopyTo(particle.Position, 0);
        }

        private void CalculateFitnessOfParticle(Swarm subswarm, Particle particle)
        {
            particle.Fitness = ObjectiveFunction.GetFitnessToObjectiveFunction(parameters.Function, particle.Position);
            if (particle.Fitness < particle.BestFitness)
            {
                particle.BestFitness = particle.Fitness;
                particle.Position.CopyTo(particle.BestPosition, 0);
                if (particle.BestFitness < subswarm.BestGlobalFitness)
                {
                    subswarm.BestGlobalFitness = particle.BestFitness;
                    particle.BestPosition.CopyTo(subswarm.BestGlobalPosition, 0);
                }
            }
        }

        private double CalculateFitnessAverageValue(Swarm subswarm)
        {
            double sum = 0.0;
            for (int i = 0; i < subswarm.Particles.Length; i++)
            {
                sum += subswarm.Particles[i].Fitness;
            }
            return sum / subswarm.Particles.Length;
        }

        public void SaveResultsOfOptimization()
        {
            results.SaveResultsToXml("MPSO");
        }
    }
}
