﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zadanie2.Data;

namespace Zadanie2.BusinessLogic
{
    class MBOA_Service
    {
        private readonly Parameters parameters;
        private int bestAmountOfIterations = int.MaxValue;
        private Results results = new Results();
        private ButterfliesPopulation butterfliesPopulation;
        private double powerExponent;

        public MBOA_Service(Parameters parameters)
        {
            this.parameters = parameters;
        }

        public void OptimizeWithMBOA()
        {
            powerExponent = HelperClass.GetRandomNumberFromRange(0.1, parameters.PowerExponent);
            List<double> averageFitnessInIteration = new List<double>();
            List<double> fitnessOfEachButterfly = new List<double>();
            List<int> iterationNumberForButterflies = new List<int>();
            butterfliesPopulation = new ButterfliesPopulation(new Butterfly[parameters.NumberOfButterflies],
                double.MaxValue, new double[parameters.NumberOfDimensions]);
            butterfliesPopulation.InitializePopulation(parameters);
            ButterfliesPopulation[] subpopulations = butterfliesPopulation.DividePopulation(parameters.NumberOfButterfliesInSubpopulation);
            double[] averageFitnessInSubpopulations = new double[subpopulations.Length];
            int iteration;
            bool optimizationWasSuccessful = false;
            for (iteration = 1; iteration < parameters.NumberOfIterations; iteration++)
            {
                for (int i = 0; i < subpopulations.Length; i++)
                {
                    OptimizeWithBOA(subpopulations[i]);
                    averageFitnessInSubpopulations[i] = CalculateFitnessAverageValue(subpopulations[i]);
                    for (int j = 0; j < subpopulations[i].Butterflies.Length; j++)
                    {
                        iterationNumberForButterflies.Add(iteration);
                        fitnessOfEachButterfly.Add(subpopulations[i].Butterflies[j].Fitness);
                    }
                }
                double averageFitnessInPopulation = averageFitnessInSubpopulations.Average();
                averageFitnessInIteration.Add(averageFitnessInPopulation);
                if (averageFitnessInPopulation < parameters.Accuracy)
                {
                    optimizationWasSuccessful = true;
                    break;
                }
                if (iteration % parameters.BOAInterval == 0)
                {
                    CombineSubpopulations(subpopulations);
                    HelperClass.Shuffle(butterfliesPopulation.Butterflies);
                    subpopulations = butterfliesPopulation.DividePopulation(parameters.NumberOfButterfliesInSubpopulation);
                }
                powerExponent = HelperClass.GetRandomNumberFromRange(0.1, parameters.PowerExponent);
            }
            if (iteration < bestAmountOfIterations)
            {
                results.FitnessFromBestOptimizationRun = averageFitnessInIteration;
                results.IterationNumberForParticles = iterationNumberForButterflies;
                results.FitnessOfEachParticle = fitnessOfEachButterfly;
                bestAmountOfIterations = iteration;
            }
            results.NumberOfIterations.Add(iteration);
            results.IfOptimizationWasSuccessful.Add(optimizationWasSuccessful);
        }

        private void CombineSubpopulations(ButterfliesPopulation[] subpopulations)
        {
            for (int i = 0; i < subpopulations.Length; i++)
            {
                for (int j = 0; j < subpopulations[i].Butterflies.Length; j++)
                {
                    butterfliesPopulation.Butterflies[i * parameters.NumberOfButterfliesInSubpopulation + j] = subpopulations[i].Butterflies[j];
                }
            }
            var subpopulationWithBestFitness = subpopulations.OrderBy(population => population.BestGlobalFitness).First();
            var index = Array.IndexOf(subpopulations, subpopulationWithBestFitness);
            butterfliesPopulation.BestGlobalFitness = subpopulations[index].BestGlobalFitness;
            subpopulations[index].BestGlobalPosition.CopyTo(butterfliesPopulation.BestGlobalPosition, 0);
        }

        private void OptimizeWithBOA(ButterfliesPopulation butterfliesPopulation)
        {
            for (int i = 0; i < butterfliesPopulation.Butterflies.Length; i++)
            {
                UpdatePositionOfButterfly(butterfliesPopulation, butterfliesPopulation.Butterflies[i]);
                CalculateFitnessOfButterfly(butterfliesPopulation, butterfliesPopulation.Butterflies[i]);
                CalculateFragranceOfButterfly(butterfliesPopulation.Butterflies[i]);
            }
        }

        private void UpdatePositionOfButterfly(ButterfliesPopulation subpopulation, Butterfly butterfly)
        {
            double[] newPosition = new double[butterfly.Position.Length];
            double random = HelperClass.GetRandomNumberFromRange(0, 1);
            double randomSquared = random * random;
            if (random < parameters.SwitchProbability)
            {
                for (int j = 0; j < butterfly.Position.Length; j++)
                {
                    newPosition[j] = butterfly.Position[j] +
                        (randomSquared * subpopulation.BestGlobalPosition[j] - butterfly.Position[j]) * butterfly.Fragrance;
                }
                newPosition.CopyTo(butterfly.Position, 0);
            }
            else
            {
                int index = HelperClass.GetRandomIndexFromTable(subpopulation.Butterflies);
                for (int j = 0; j < butterfly.Position.Length; j++)
                {
                    newPosition[j] = butterfly.Position[j] + (randomSquared * subpopulation.Butterflies[index].Position[j]
                        - butterfly.Position[j]) * butterfly.Fragrance;
                }
                newPosition.CopyTo(butterfly.Position, 0);
            }
        }

        private void CalculateFitnessOfButterfly(ButterfliesPopulation subpopulation, Butterfly butterfly)
        {
            butterfly.Fitness = ObjectiveFunction.GetFitnessToObjectiveFunction(parameters.Function, butterfly.Position);
            if (butterfly.Fitness < subpopulation.BestGlobalFitness)
            {
                subpopulation.BestGlobalFitness = butterfly.Fitness;
                butterfly.Position.CopyTo(subpopulation.BestGlobalPosition, 0);
            }
        }

        private void CalculateFragranceOfButterfly(Butterfly butterfly)
        {
            butterfly.Fragrance = parameters.ModularModality * Math.Pow(butterfly.Fitness, powerExponent);
        }

        private double CalculateFitnessAverageValue(ButterfliesPopulation subpopulation)
        {
            double sum = 0.0;
            for (int i = 0; i < subpopulation.Butterflies.Length; i++)
            {
                sum += subpopulation.Butterflies[i].Fitness;
            }
            return sum / subpopulation.Butterflies.Length;
        }

        public void SaveResultsOfOptimization()
        {
            results.SaveResultsToXml("MBOA");
        }
    }
}
