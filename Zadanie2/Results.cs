﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Zadanie2
{
    class Results
    {
        public List<double> FitnessFromBestOptimizationRun { get; set; } = new List<double>();
        public List<int> NumberOfIterations { get; set; } = new List<int>();
        public List<bool> IfOptimizationWasSuccessful { get; set; } = new List<bool>();
        public List<double> FitnessOfEachParticle { get; set; } = new List<double>();
        public List<int> IterationNumberForParticles { get; set; } = new List<int>();

        private void SaveFitnessFromBestOptimizationRunToXml(string algorithmName)
        {
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "/" + algorithmName + "_fitnessFromBestOptimizationRun.xml";
            using (StreamWriter sw = new StreamWriter(filepath))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<double>));
                xmlSerializer.Serialize(sw, FitnessFromBestOptimizationRun);
            }
        }

        private void SaveNumberOfIterationsToXml(string algorithmName)
        {
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "/" + algorithmName + "_numberOfIterations.xml";
            using (StreamWriter sw = new StreamWriter(filepath))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<int>));
                xmlSerializer.Serialize(sw, NumberOfIterations);
            }
        }

        private void SaveIfOptimizationWasSuccessfulToXml(string algorithmName)
        {
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "/" + algorithmName + "_ifOptimizationWasSuccessful.xml";
            using (StreamWriter sw = new StreamWriter(filepath))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<bool>));
                xmlSerializer.Serialize(sw, IfOptimizationWasSuccessful);
            }
        }

        private void SaveIterationNumberForParticlesToXml(string algorithmName)
        {
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "/" + algorithmName + "_iterationNumberForParticles.xml";
            using (StreamWriter sw = new StreamWriter(filepath))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<int>));
                xmlSerializer.Serialize(sw, IterationNumberForParticles);
            }
        }

        private void SaveFitnessOfEachParticleToXml(string algorithmName)
        {
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "/" + algorithmName + "_fitnessOfEachParticle.xml";
            using (StreamWriter sw = new StreamWriter(filepath))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<double>));
                xmlSerializer.Serialize(sw, FitnessOfEachParticle);
            }
        }

        public void SaveResultsToXml(string algorithmName)
        {
            SaveFitnessFromBestOptimizationRunToXml(algorithmName);
            SaveNumberOfIterationsToXml(algorithmName);
            SaveIfOptimizationWasSuccessfulToXml(algorithmName);
            SaveIterationNumberForParticlesToXml(algorithmName);
            SaveFitnessOfEachParticleToXml(algorithmName);
        }
    }
}
