﻿using Zadanie2.BusinessLogic;

namespace Zadanie2.Data
{
    class Swarm
    {
        public Particle[] Particles { get; set; }
        public double[] BestGlobalPosition { get; set; }
        public double BestGlobalFitness { get; set; }

        public Swarm(Particle[] particles, double[] bestGlobalPosition, double bestGlobalFitness)
        {
            Particles = particles;
            BestGlobalPosition = bestGlobalPosition;
            BestGlobalFitness = bestGlobalFitness;
        }

        public void InitializeSwarm(Parameters parameters)
        {
            for (int i = 0; i < Particles.Length; i++)
            {
                double[] randomPosition = new double[parameters.NumberOfDimensions];
                double[] randomVelocity = new double[parameters.NumberOfDimensions];
                for (int j = 0; j < parameters.NumberOfDimensions; j++)
                {
                    randomPosition[j] = HelperClass.GetRandomNumberFromRange(parameters.MinValue, parameters.MaxValue);
                    randomVelocity[j] = HelperClass.GetRandomNumberFromRange(parameters.MinValue * 0.1, parameters.MaxValue * 0.1);
                }
                double fitness = ObjectiveFunction.GetFitnessToObjectiveFunction(parameters.Function, randomPosition);

                Particles[i] = new Particle(randomPosition, randomVelocity, fitness, randomPosition, fitness);
                if (Particles[i].Fitness < BestGlobalFitness)
                {
                    BestGlobalFitness = Particles[i].Fitness;
                    Particles[i].Position.CopyTo(BestGlobalPosition, 0);
                }
            }
        }

        public Swarm[] DivideSwarmIntoSubswarms(int p)
        {
            int numberOfSubswarms = Particles.Length / p;
            Swarm[] subswarms = new Swarm[numberOfSubswarms];
            for (int i = 0; i < numberOfSubswarms; i++)
            {
                Particle[] particles = new Particle[p];
                for (int j = 0; j < p; j++)
                {
                    particles[j] = Particles[i * p + j];
                }
                subswarms[i] = new Swarm(particles, BestGlobalPosition, BestGlobalFitness);
            }
            return subswarms;
        }
    }
}
