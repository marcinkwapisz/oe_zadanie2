﻿namespace Zadanie2.Data
{
    class Butterfly
    {
        public double[] Position { get; set; }
        public double Fitness { get; set; }
        public double Fragrance { get; set; }

        public Butterfly(double[] position, double fitness, double fragrance)
        {
            Position = new double[position.Length];
            position.CopyTo(Position, 0);
            Fitness = fitness;
            Fragrance = fragrance;
        }
    }
}
