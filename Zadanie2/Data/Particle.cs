﻿namespace Zadanie2.Data
{
    class Particle
    {
        public double[] Position { get; set; }
        public double[] Velocity { get; set; }
        public double Fitness { get; set; }
        public double[] BestPosition { get; set; }
        public double BestFitness { get; set; }

        public Particle(double[] position, double[] velocity, double fitness, double[] bestPosition, double bestFitness)
        {
            Position = new double[position.Length];
            position.CopyTo(Position, 0);
            Velocity = new double[velocity.Length];
            velocity.CopyTo(Velocity, 0);
            Fitness = fitness;
            BestPosition = new double[bestPosition.Length];
            bestPosition.CopyTo(BestPosition, 0);
            BestFitness = bestFitness;
        }
    }
}
