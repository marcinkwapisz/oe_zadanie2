﻿using System;
using Zadanie2.BusinessLogic;

namespace Zadanie2.Data
{
    class ButterfliesPopulation
    {
        public Butterfly[] Butterflies { get; set; }
        public double BestGlobalFitness { get; set; }
        public double[] BestGlobalPosition { get; set; }

        public ButterfliesPopulation(Butterfly[] butterflies, double bestGlobalFitness, double[] bestGlobalPosition)
        {
            Butterflies = butterflies;
            BestGlobalFitness = bestGlobalFitness;
            BestGlobalPosition = bestGlobalPosition;
        }

        public void InitializePopulation(Parameters parameters)
        {
            for (int i = 0; i < Butterflies.Length; i++)
            {
                double[] randomPosition = new double[parameters.NumberOfDimensions];
                for (int j = 0; j < parameters.NumberOfDimensions; j++)
                {
                    randomPosition[j] = HelperClass.GetRandomNumberFromRange(parameters.MinValue, parameters.MaxValue);
                }
                double fitness = ObjectiveFunction.GetFitnessToObjectiveFunction(parameters.Function, randomPosition);
                double fragrance = parameters.ModularModality * Math.Pow(fitness, parameters.PowerExponent);
                Butterflies[i] = new Butterfly(randomPosition, fitness, fragrance);
                if (Butterflies[i].Fitness < BestGlobalFitness)
                {
                    BestGlobalFitness = Butterflies[i].Fitness;
                    Butterflies[i].Position.CopyTo(BestGlobalPosition, 0);
                }
            }
        }

        public ButterfliesPopulation[] DividePopulation(int p)
        {
            int numberOfSubpopulations = Butterflies.Length / p;
            ButterfliesPopulation[] subpopulations = new ButterfliesPopulation[numberOfSubpopulations];
            for (int i = 0; i < numberOfSubpopulations; i++)
            {
                Butterfly[] butterflies = new Butterfly[p];
                for (int j = 0; j < p; j++)
                {
                    butterflies[j] = Butterflies[i * p + j];
                }
                subpopulations[i] = new ButterfliesPopulation(butterflies, BestGlobalFitness, BestGlobalPosition);
            }
            return subpopulations;
        }
    }
}
