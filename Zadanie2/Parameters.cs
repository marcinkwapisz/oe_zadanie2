﻿using static Zadanie2.BusinessLogic.ObjectiveFunction;

namespace Zadanie2
{
    public class Parameters
    {
        public int NumberOfRepetitions { get; set; }
        public FunctionTypeEnum Function { get; set; }
        public int NumberOfDimensions { get; set; }
        public double MinValue { get; set; }
        public double MaxValue { get; set; }
        public int NumberOfIterations { get; set; }
        public double Accuracy { get; set; }

        public int NumberOfParticles { get; set; }
        public double Weight { get; set; }
        public double CognitiveWeight { get; set; }
        public double SocialWeight { get; set; }
        public int MPSOInterval { get; set; }
        public int NumberOfParticlesInSubswarm { get; set; }

        public int NumberOfButterflies { get; set; }
        public double ModularModality { get; set; }
        public double PowerExponent { get; set; }
        public double SwitchProbability { get; set; }
        public int BOAInterval { get; set; }
        public int NumberOfButterfliesInSubpopulation { get; set; }

        public Parameters(int numberOfRepetitions, FunctionTypeEnum function, int numberOfDimensions,
            double minValue, double maxValue, int numberOfIterations, double accuracy, int numberOfParticles, 
            double weight, double cognitiveWeight, double socialWeight, int mPSOInterval, int numberOfParticlesInSubswarm)
        {
            NumberOfRepetitions = numberOfRepetitions;
            Function = function;
            NumberOfDimensions = numberOfDimensions;
            MinValue = minValue;
            MaxValue = maxValue;
            NumberOfIterations = numberOfIterations;
            Accuracy = accuracy;
            NumberOfParticles = numberOfParticles;
            Weight = weight;
            CognitiveWeight = cognitiveWeight;
            SocialWeight = socialWeight;
            MPSOInterval = mPSOInterval;
            NumberOfParticlesInSubswarm = numberOfParticlesInSubswarm;
        }

        public Parameters(int numberOfRepetitions, int numberOfDimensions, FunctionTypeEnum function,
            double minValue, double maxValue, int numberOfIterations, double accuracy, int numberOfButterflies, 
            double modularModality, double powerExponent, double switchProbability, int boaInterval, int numberOfButterfliesInSubpopulation)
        {
            NumberOfRepetitions = numberOfRepetitions;
            Function = function;
            NumberOfDimensions = numberOfDimensions;
            MinValue = minValue;
            MaxValue = maxValue;
            NumberOfIterations = numberOfIterations;
            Accuracy = accuracy;
            NumberOfButterflies = numberOfButterflies;
            ModularModality = modularModality;
            PowerExponent = powerExponent;
            SwitchProbability = switchProbability;
            BOAInterval = boaInterval;
            NumberOfButterfliesInSubpopulation = numberOfButterfliesInSubpopulation;
        }

        public Parameters()
        {
        }
    }
}
