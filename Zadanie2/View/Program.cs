﻿using System;
using System.IO;
using System.Xml.Serialization;
using Zadanie2.BusinessLogic;

namespace Zadanie2
{
    class Program
    {
        static void Main(string[] args)
        {
            //string parametersPath = AppDomain.CurrentDomain.BaseDirectory + "../../PSO_parameters.xml";
            string parametersPath = AppDomain.CurrentDomain.BaseDirectory + "../../BOA_parameters.xml";

            Parameters retrievedParameters = new Parameters();
            using (FileStream fileStream = new FileStream(parametersPath, FileMode.Open))
            {
                XmlSerializer serializer = new XmlSerializer(retrievedParameters.GetType());
                retrievedParameters = serializer.Deserialize(fileStream) as Parameters;
            }

            //MPSO_Service mPSO_Service = new MPSO_Service(retrievedParameters);
            //for (int i = 0; i < retrievedParameters.NumberOfRepetitions; i++)
            //{
            //    mPSO_Service.OptimizeWithMPSO();
            //}
            //mPSO_Service.SaveResultsOfOptimization();

            //MSPSO_Service msPSO_Service = new MSPSO_Service(retrievedParameters);
            //for (int i = 0; i < retrievedParameters.NumberOfRepetitions; i++)
            //{
            //    msPSO_Service.OptimizeWithMSPSO();
            //}
            //msPSO_Service.SaveResultsOfOptimization();

            MBOA_Service mBOA_Service = new MBOA_Service(retrievedParameters);
            for (int i = 0; i < retrievedParameters.NumberOfRepetitions; i++)
            {
                mBOA_Service.OptimizeWithMBOA();
            }
            mBOA_Service.SaveResultsOfOptimization();
        }
    }
}
