## MSPSO, MPSO, MBOA
This project was written using .NET Framework 4.6.1.

It consists of the self-written code of various bio-inspired, multi-swarm evolutionary calculation methods:
* MSPSO (Multi Swarm Particle Swarm Optimization)
* MPSO (Modified Particle Swarm Optimization)
* MBOA (Modified Butterfly Optimization Algorithm)

The main goal was to find minimum values of functions of several variables (Rosenbrock, Griewank, Rastrigin) and comparison of the effectiveness and results of given algorithms.
